#!/usr/bin/python

# this file must be executable, use command chmod +x <filename>
# this file must be in cgi-bin directory

import csv

input_file = open('cgi-bin/Nawapon Jeanpanichpong.csv')
data = csv.DictReader(input_file)  # read using DictReader

count1 = 0;
for row in data:
    # print row['Date'], row['Subject']
    if row['Subject'] == 'Com':
        count1 = count1 + 1

input_file = open('cgi-bin/Songwut Butsridoung.csv')
data = csv.DictReader(input_file)  # read using DictReader

count2 = 0;
for row in data:
    # print row['Date'], row['Subject']
    if row['Subject'] == 'Com':
        count2 = count2 + 1

input_file = open('cgi-bin/Pathompat Sungpankhao.csv')
data = csv.DictReader(input_file)  # read using DictReader

count3 = 0;
for row in data:
    # print row['Date'], row['Subject']
    if row['Subject'] == 'Com':
        count3 = count3 + 1

input_file = open('cgi-bin/Chayanin Khawsanit.csv')
data = csv.DictReader(input_file)  # read using DictReader

count4 = 0;
for row in data:
    # print row['Date'], row['Subject']
    if row['Subject'] == 'Com':
        count4 = count4 + 1


input_file = open('cgi-bin/Nattapong Vinyunuluk.csv')
data = csv.DictReader(input_file)  # read using DictReader
count5 = 0;
for row in data:
    # print row['Date'], row['Subject']
    if row['Subject'] == 'Com':
        count5 = count5 + 1




input_file.close()

# print count


# HTML output
# just print out the text/html

print "Content-type:text/html\r\n\r\n"
print "<html>"
print "<head>"
print "<title>Intro-Com Count</title>"
print "<style>"
print"table {border-collapse: collapse;}"
print"table, td, th {border: 1px solid black;}"
print "body {background-color: Bisque  ;}"
print"</style>"
print "</head>"
print "<body>"
print "<h1>Introcom</h1>"
print "<table>"
print " </tr>"
print " <tr>"
print "<th>Name</th>"
print "<th>Times Reading</th>"
print " </tr>"
print " <tr>"
print "<td>Nawapon Jeanpanichpong</td>"
print "<td>",count1,"</td>"
print " </tr>"
print " <tr>"
print "<td>Songwut Butsridoung</td>"
print "<td>",count2,"</td>"
print " </tr>"
print " <tr>"
print "<td>Pathompat Sungpankhao</td>"
print "<td>",count3,"</td>"
print " </tr>"
print " <tr>"
print "<td>Chayanin Khawsanit</td>"
print "<td>",count4,"</td>"
print " </tr>"
print " <tr>"
print "<td>Nattapong Vinyunuluk</td>"
print "<td>",count5,"</td>"
print " </tr>"
print "</table>"

# In Python, we can use " or ' for string
print '<svg width="800" height="600">'

print '  <text x="420"y="120" > Nawapon Jeanpanichpong </text>' 
print '  <text x="420"y="220" > Songwut Butsridoung </text>' 
print '  <text x="420"y="320" > Pathompat Sungpankhao </text>' 
print '  <text x="420"y="420" > Chayanin Khawsanit </text>' 
print '  <text x="420"y="520" > Nattapong Vinyunuluk </text>' 
print ' <rect x=',400 - (10*count1),' y="100" height= "50" width = ',(10*count1),' style="fill: red" />'
print ' <rect x=',400 - (10*count2),' y="200" height= "50" width = ',(10*count2),' style="fill: purple" />'
print ' <rect x=',400 - (10*count3),' y="300" height= "50" width = ',(10*count3),' style="fill: gold" />'
print ' <rect x=',400 - (10*count4),' y="400" height= "50" width = ',(10*count4),' style="fill: silver" />'
print ' <rect x=',400 - (10*count5),' y="500" height= "50" width = ',(10*count5),' style="fill: green" />'
print '<line x1="400" y1="80" x2="400" y2="560" style="stroke:rgb(255,0,0);stroke-width:2" />'
print '  <text x=',400 - (10*count1),'y="120" >',count1,'</text>' 
print '  <text x=',400 - (10*count2),'y="220" >',count2,'</text>' 
print '  <text x=',400 - (10*count3),'y="320" >',count3,'</text>' 
print '  <text x=',400 - (10*count4),'y="420" >',count4,'</text>' 
print '  <text x=',400 - (10*count5),'y="520" >',count5,'</text>' 

print '</svg>'
print '<a href="http://0.0.0.0:8000/cgi-bin/home.py"> << Back </a>'
print "</body>"
print "</html>"